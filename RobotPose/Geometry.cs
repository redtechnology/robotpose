﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    public class Geometry
    {
        private Geometry()
        {
        }

        public static Pose PoseInv(Pose original)
        {
            Pos reversePos = -original.Pos;

            PosVector reversePosVector = reversePos;

            OrientMatrix originalOrientMatrix = original.Orient;

            PosVector inversePosVector = originalOrientMatrix * reversePosVector;

            Pose inverse = new Pose();
            inverse.Pos = inversePosVector;
            inverse.Orient = original.Orient.Conjugate;

            return inverse;
        }

        public static Pose PoseMult(Pose first, Pose second)
        {
            PosVector secondPosVector = second.Pos;

            OrientMatrix firstOrientConjugateMatrix = first.Orient.Conjugate;

            PosVector transformedSecondPosVector = firstOrientConjugateMatrix * secondPosVector;

            Pose product = new Pose();
            product.Pos = first.Pos + transformedSecondPosVector;
            product.Orient = first.Orient * second.Orient;

            return product;
        }
    }
}
