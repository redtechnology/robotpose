﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    public class Pose
    {
        private Pos _pos;
        private Orient _orient;

        public Pose(Pos pos, Orient orient)
        {
            this._pos = pos;
            this._orient = orient;
        }

        public Pose()
        {
            this._pos = new Pos();
            this._orient = new Orient();
        }

        public Pos Pos
        {
            get 
            {
                return this._pos;
            }
            set
            {
                this._pos = value;
            }
        }

        public Orient Orient
        {
            get
            {
                return this._orient;
            }
            set
            {
                this._orient = value;
            }
        }
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Pose p = obj as Pose;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Pos == p.Pos) && (Orient == p.Orient);
        }

        public bool Equals(Pose p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Pos == p.Pos) && (Orient == p.Orient);
        }

        public override int GetHashCode()
        {
            return Pos.GetHashCode() ^ Orient.GetHashCode();
        }

        public static bool operator ==(Pose a, Pose b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Pos == b.Pos && a.Orient == b.Orient;
        }

        public static bool operator !=(Pose a, Pose b)
        {
            return !(a == b);
        }
    }

}
