﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{

    public class Vector : Matrix
    {
        protected Vector(int rows, int columns) : base(rows, columns)
        {
        }
        public Vector(Vector other) : base(other)
        {
        }

        public static Vector ColumnVector(int rows)
        {
            return new Vector(rows, 1);
        }
        public static Vector RowVector(int columns)
        {
            return new Vector(1, columns);
        }

        public double this[int element]
        {
            get
            {
                if (_rows == 1)
                {
                    return _matrix[0, element];
                }
                else
                {
                    return _matrix[element, 0];
                }
            }
            set
            {
                if (_rows == 1)
                {
                    _matrix[0, element] = value;
                }
                else
                {
                    _matrix[element, 0] = value;
                }
                
            }
        }

        public static double DotProduct(Vector a, Vector b)
        {
            if (a.Columns != b.Rows)
            {
                throw new VectorException("Dot Product cannot be calculated: Incorrect number of rows and/or columns");
            }
            if (a.Rows != b.Columns)
            {
                throw new VectorException("Dot Product cannot be calculated: Incorrect number of rows and/or columns");
            }
            
            Matrix resultantMatrix = a * b;
            double result = resultantMatrix[0, 0];
			
            return result;
        }
        public static Vector CrossProduct(Vector a, Vector b)
        {
            if (a.Columns != b.Columns)
            {
                throw new VectorException("Cross Product cannot be calculated: Incorrect number of rows and/or columns");
            }
            if (a.Rows != b.Rows)
            {
                throw new VectorException("Cross Product cannot be calculated: Incorrect number of rows and/or columns");
            }
            if (a.Rows != 1)
            {
                throw new VectorException("Cross Product cannot be calculated: 1 row and 3 columns in first matrix and second matrix permissible");
            }
            if (a.Columns != 3)
            {
                throw new VectorException("Cross Product cannot be calculated: 1 row and 3 columns in first matrix and second matrix permissible");
            }
            double ax = a[0, 0];
            double ay = a[0, 1];
            double az = a[0, 2];

            double bx = b[0, 0];
            double by = b[0, 1];
            double bz = b[0, 2];

            double cx = ay * bz - az * by;
            double cy = az * bx - ax * bz;
            double cz = ax * by - ay * bx;

            Vector crossProduct = new Vector(1, 3);

            crossProduct[0] = cx;
            crossProduct[1] = cy;
            crossProduct[2] = cz;
         
            return crossProduct;
        }

        public class VectorException : Exception
        {
            public VectorException()
            {
            }
            public VectorException(string message)
                : base(message)
            {
            }
            public VectorException(string message, Exception inner)
                : base(message, inner)
            {
            }

        }
    }
}
