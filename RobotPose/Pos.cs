﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    public class Pos
    {
        private double _x;
        private double _y;
        private double _z;

        public Pos(double x, double y, double z)
        {
            this._x = x;
            this._y = y;
            this._z = z;
        }

        public Pos()
        {
            this._x = 0;
            this._y = 0;
            this._z = 0;
        }

        public Pos(Pos other)
        {
            this._x = other.X;
            this._y = other.Y;
            this._z = other.Z;
        }
        
        public Pos(Vector vector) 
        {
            if (!(vector.Rows == 1 && vector.Columns == 3) && !(vector.Rows == 3 && vector.Columns == 1))
            {
                throw new Vector.VectorException("Vector must only have 3 elements");
            }
            _x = vector[0];
            _y = vector[1];
            _z = vector[2];
        }

        // This is the assignment operator ("="). Converts Vector to Pos.
        public static implicit operator Pos(Vector vector)
        {
            return new Pos(vector);
        }
        public double X
        {
            get
            {
                return this._x;
            }
            set
            {
                this._x = value;
            }
        }

        public double Y
        {
            get
            {
                return this._y;
            }
            set
            {
                this._y = value;
            }
        }

        public double Z
        {
            get
            {
                return this._z;
            }
            set
            {
                this._z = value;
            }
        }
        public override string ToString()
        {
            return X + ", " + Y + ", " + Z;
        }
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Pos p = obj as Pos;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y) && (Z == p.Z);
        }

        public bool Equals(Pos p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y) && (Z == p.Z);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();
        }

        public static bool operator ==(Pos a, Pos b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }

        public static bool operator !=(Pos a, Pos b)
        {
            return !(a == b);
        }

        public static Pos operator +(Pos a, Pos b)
        {
            return new Pos(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Pos operator -(Pos a, Pos b)
        {
            return new Pos(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Pos operator -(Pos a)
        {
            return new Pos(-a.X, -a.Y, -a.Z);
        }    
    }

}
