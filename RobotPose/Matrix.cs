﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    public class Matrix
    {
        protected int _rows;
        protected int _columns;

        protected double[,] _matrix;

        public int Rows
        {
            get
            {
                return _rows;
            }
        }

        public int Columns
        {
            get
            {
                return _columns;
            }
        }

        public double this[int row, int column]
        {
            get
            {
                return _matrix[row, column];
            }
            set
            {
                _matrix[row, column] = value;
            }
        }

        public Matrix(Matrix other)
        {
            _matrix = new double[other.Rows, other.Columns];

            _rows = other.Rows;
            _columns = other.Columns;

            Array.Copy(other._matrix, this._matrix, other._matrix.Length);
        }

        public Matrix(int rows, int columns)
        {
            _matrix = new double[rows, columns];

            _rows = rows;
            _columns = columns;

            for (int row = 0; row < _rows; row++)
            {
                for (int column = 0; column < _columns; column++)
                {
                    _matrix[row, column] = 0;
                }

            }
        }

        public Matrix(double[,] array2D)
        {
            const int rowDimension = 0;
            const int columnDimension = 1;

            _rows = array2D.GetLength(rowDimension);
            _columns = array2D.GetLength(columnDimension);

            _matrix = new double[_rows, _columns];

            Array.Copy(array2D, _matrix, array2D.Length);
        }

        // This is the assignment operator ("="). Converts 2D array of doubles to matrix.
        public static implicit operator Matrix(double[,] array2D)
        {
            return new Matrix(array2D);
        }

        public Matrix Transpose()
        {
            Matrix transpose = new Matrix(this.Columns, this.Rows);

            for (int row = 0; row < this.Rows; row++)
            {
                for (int column = 0; column < this.Columns; column++)
                {
                    transpose[column, row] = this[row, column];
                }

            }

            return transpose;
        }

        // this where I want my multiply operator to matrix multiply two matrices together
        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.Columns != b.Rows)
            {
                throw new MatrixException("Matrices cannot be multiplied: Incorrect number of rows and/or columns");
            }

            Matrix product = new Matrix(a.Rows, b.Columns);

            for (int row = 0; row < product.Rows; ++row)
            {
                for (int col = 0; col < product.Columns; ++col)
                {
                    product[row, col] = 0.0;
                    for (int inner = 0; inner < a.Columns; ++inner)
                    {
                        product[row, col] += a[row, inner] * b[inner, col];
                    }
                }
            }

            return product;
        }

        public class MatrixException : Exception
        {
            public MatrixException()
            {
            }

            public MatrixException(string message)
                : base(message)
            {
            }

            public MatrixException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

    }
    
}
