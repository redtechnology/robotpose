﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    class OrientMatrix : Matrix
    {
        public OrientMatrix(Orient orient) : base(3, 3)
        {
            if (!Orient.IsNormalized(orient))
            {
                throw new OrientException("Orient quaternion must be normalized");
            }

            double w = orient.Q1;
            double x = orient.Q2;
            double y = orient.Q3;
            double z = orient.Q4;

            // The following equations are from
            // https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

            double n = w * w + x * x + y * y + z * z;
            double s = 0.0;

            if (n > 0.0)
            {
                s = 2.0 / n;
            }
            double wx = s * w * x;
            double wy = s * w * y;
            double wz = s * w * z;
            double xx = s * x * x;
            double xy = s * x * y;
            double xz = s * x * z;
            double yy = s * y * y;
            double yz = s * y * z;
            double zz = s * z * z;

            _matrix[0, 0] = 1.0 - (yy + zz);
            _matrix[0, 1] = xy + wz;
            _matrix[0, 2] = xz - wy;

            _matrix[1, 0] = xy - wz;
            _matrix[1, 1] = 1.0 - (xx + zz);
            _matrix[1, 2] = yz + wx;

            _matrix[2, 0] = xz + wy;
            _matrix[2, 1] = yz - wx;
            _matrix[2, 2] = 1.0 - (xx + yy);
        }

        // This is the assignment operator ("="). Converts Orient to Matrix.
        public static implicit operator OrientMatrix(Orient orient)
        {
            return new OrientMatrix(orient);
        }
    }
}
