﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    class PosVector : Vector
    {
        public PosVector(Pos pos) : base(ColumnVector(3))
        {
            this[0] = pos.X;
            this[1] = pos.Y;
            this[2] = pos.Z;
        }
        // This is the assignment operator ("="). Converts Pos to PosVector.
        public static implicit operator PosVector(Pos pos)
        {
            return new PosVector(pos);
        }
        public PosVector(Matrix matrix) : base(ColumnVector(3))
        {
            if (matrix.Rows == 1 && matrix.Columns == 3)
            {
                this[0] = matrix[0, 0];
                this[1] = matrix[0, 1];
                this[2] = matrix[0, 2];
            }
            else if (matrix.Rows == 3 && matrix.Columns == 1)
            {
                this[0] = matrix[0, 0];
                this[1] = matrix[1, 0];
                this[2] = matrix[2, 0];
            }
            else
            {
                throw new VectorException("PosVector cannot be instantiated: Incorrect number of rows and/or columns in matrix");
            }

        }

        public static PosVector operator *(OrientMatrix a, PosVector b)
        {
            Matrix product = (Matrix)a * (Matrix)b;

            PosVector vector = new PosVector(product);

            return vector;
        }
    }
}
