﻿﻿// Copyright 2019 Dr Derek R Charles
// 
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//  
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotPose
{
    public class Orient
    {
        private double _q1;
        private double _q2;
        private double _q3;
        private double _q4;

        public Orient(double q1, double q2, double q3, double q4)
        {
            if (!IsNormalized(q1, q2, q3, q4))
            {
                throw new OrientException("Invalid quaternion");
            }

            this._q1 = q1;
            this._q2 = q2;
            this._q3 = q3;
            this._q4 = q4;
        }

        public Orient()
        {
            this._q1 = 1.0;
            this._q2 = 0.0;
            this._q3 = 0.0;
            this._q4 = 0.0;
        }

        public Orient(Orient other)
        {
            this._q1 = other.Q1;
            this._q2 = other.Q2;
            this._q3 = other.Q3;
            this._q4 = other.Q4;
        }
        public Orient(Matrix matrix)
        {
            if (matrix.Columns != 3)
            {
                throw new OrientException("Rotation matrix must have 3 columns");
            }
            if (matrix.Rows != 3)
            {
                throw new OrientException("Rotation matrix must have 3 rows");
            }

            double x1 = matrix[0, 0];
            double x2 = matrix[1, 0];
            double x3 = matrix[2, 0];

            double y1 = matrix[0, 1];
            double y2 = matrix[1, 1];
            double y3 = matrix[2, 1];

            double z1 = matrix[0, 2];
            double z2 = matrix[1, 2];
            double z3 = matrix[2, 2];

            // The following equations are from
            // the RAPID reference manual 3HAC 7783-1 / 7774-1
            // and are equivalent to formulas in:
            // https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

            double w = Math.Sqrt(x1 + y2 + z3 + 1) / 2;
            double x = CopySign(Math.Sqrt(x1 - y2 - z3 + 1) / 2, y3 - z2);
            double y = CopySign(Math.Sqrt(y2 - x1 - z3 + 1) / 2, z1 - x3);
            double z = CopySign(Math.Sqrt(z3 - x1 - y2 + 1) / 2, x2 - y1);

            if (!IsNormalized(w, x, y, z))
            {
                throw new OrientException("Invalid quaternion");
            }

            _q1 = w;
            _q2 = x;
            _q3 = y;
            _q4 = z;
        }
        private static double CopySign(double x, double y)
        {
            double sign = 1.0;
            if (y < 0.0)
            {
                sign = -1.0;
            }
            double modulusX = Math.Abs(x);

            return sign * modulusX;
        }
        // This is the assignment operator ("="). Converts Matrix to Orient.
        public static implicit operator Orient(Matrix matrix)
        {
            return new Orient(matrix);
        }

        public double Q1
        {
            get
            {
                return this._q1;
            }
        }

        public double Q2
        {
            get
            {
                return this._q2;
            }
        }

        public double Q3
        {
            get
            {
                return this._q3;
            }
        }

        public double Q4
        {
            get
            {
                return this._q4;
            }
        }
        public override string ToString()
        {
            return Q1 + ", " + Q2 + ", " + Q3 + ", " + Q4;
        }
        
        public Orient Conjugate
        {
            get
            {
                Orient conjugate = new Orient(Q1, -Q2, -Q3, -Q4);
                return conjugate;
            }
        }

        public void Normalize()
        {
            double vectorLength = Math.Sqrt((Q1 * Q1) + (Q2 * Q2) + (Q3 * Q3) + (Q4 * Q4));

            _q1 /= vectorLength;
            _q2 /= vectorLength;
            _q3 /= vectorLength;
            _q4 /= vectorLength;
        }

        public static bool IsNormalized(Orient orient)
        {
            return IsNormalized(orient.Q1, orient.Q2, orient.Q3, orient.Q4);
        }

        public static bool IsNormalized(double q1, double q2, double q3, double q4)
        {
            bool isNormalized = false;

            double vectorLength = Math.Sqrt( (q1 * q1) + (q2 * q2) + (q3 * q3) + (q4 * q4) );

            const double unitLength = 1.0;

            const double allowableTolerance = 0.000001;

            if (Math.Abs(vectorLength - unitLength) < allowableTolerance)
            {
                isNormalized = true;
            }

            return isNormalized;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Orient orient = obj as Orient;
            if ((System.Object)orient == null)
            {
                return false;
            }

            // Return true if the fields match:
            return ( (Q1 == orient.Q1) && (Q2 == orient.Q2) && (Q3 == orient.Q3) && (Q4 == orient.Q4) );
        }

        public bool Equals(Orient orient)
        {
            // If parameter is null return false:
            if ((object)orient == null)
            {
                return false;
            }

            // Return true if the fields match:
            return ((Q1 == orient.Q1) && (Q2 == orient.Q2) && (Q3 == orient.Q3) && (Q4 == orient.Q4));
        }

        public override int GetHashCode()
        {
            return Q1.GetHashCode() ^ Q2.GetHashCode() ^ Q3.GetHashCode() ^ Q4.GetHashCode();
        }

        public static bool operator ==(Orient a, Orient b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return ( (a.Q1 == b.Q1) && (a.Q2 == b.Q2) && (a.Q3 == b.Q3) && (a.Q4 == b.Q4) );
        }

        public static bool operator !=(Orient a, Orient b)
        {
            return !(a == b);
        }

        public static Orient operator *(Orient a, Orient b)
        {
            Orient resultant = new Orient();

            resultant._q1 = (b.Q1 * a.Q1) - (b.Q2 * a.Q2) - (b.Q3 * a.Q3) - (b.Q4 * a.Q4);

            resultant._q2 = (b.Q1 * a.Q2) + (b.Q2 * a.Q1) - (b.Q3 * a.Q4) + (b.Q4 * a.Q3);

            resultant._q3 = (b.Q1 * a.Q3) + (b.Q2 * a.Q4) + (b.Q3 * a.Q1) - (b.Q4 * a.Q2);

            resultant._q4 = (b.Q1 * a.Q4) - (b.Q2 * a.Q3) + (b.Q3 * a.Q2) + (b.Q4 * a.Q1);

            return resultant;
        }
    }

    public class OrientException : Exception
    {
        public OrientException()
        {
        }

        public OrientException(string message)
            : base(message)
        {
        }

        public OrientException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
