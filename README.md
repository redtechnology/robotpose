# RobotPose

A simple C# library which reproduces some ABB geometrical functions and structures as used on the robots in the RAPID programming language, enabling developers to perform offline geometrical calculations and conversions outside of the FlexPendant or RobotStudio environments.

## RAPID structures

### Pos

Represents a 3D point relative to a co-ordinate system.
```
Pos(double x, double y, double z)
```
Can also be initialized with, or implicitly converted to, a column vector for use with a transformation matrix or other vector operations.

### Orient
Represents a quaternion, used for orientations or rotations relative to a co-ordinate system
```
public Orient(double q1, double q2, double q3, double q4)
```
Can also be initialized with, or implicitly converted to, a rotation matrix.

### Pose
Combines a Pos and Orient to define a point and orientation relative to a co-ordinate system.
```
public Pose(Pos pos, Orient orient)
```

## RAPID functions

### PoseInv
PoseInv (Pose Invert) calculates the reverse transformation of a pose.
```
public static Pose PoseInv(Pose original)
```
A function with a return value of the data type Pose.

Pose1 represents the coordinates system 1 related to the coordinate system 0.
![PoseInv image](/images/PoseInv.png)

The transformation giving the coordinate system 0 related to the coordinate system 1 is obtained by the reverse transformation, stored in pose2:

```
Pose pose1;
Pose pose2;
...
Pose pose2 = PoseInv(pose1);
```

### PoseMult
PoseMult (Pose Multiply) is used to calculate the product of two pose transformations.
A typical use is to calculate a new pose as the result of a displacement acting on an original pose.
```
public static Pose PoseMult(Pose first, Pose second)
```
A function with a return value of the data type Pose.

pose1 represents the coordinate system 1 related to the coordinate system 0.

pose2 represents the coordinate system 2 related to the coordinate system 1.
![PoseMult image](/images/PoseMult.png)

The transformation giving pose3, the coordinate system 2 related to the coordinate system 0, is obtained by the product of the two transformations:

```
Pose pose1;
Pose pose2;
Pose pose2;
...
Pose pose3 = PoseMult(pose1, pose2);
```
## Additional support

### Matrix
Generic MxN matrix class.
Supports initialization from a 2D arrays, performs matrix multiplication and transposition.

### OrientMatrix
Specialized 3x3 matrix class derived from Matrix.
Helper class used to convert a quaternion (Orient) to a 3x3 rotation matrix

### Vector
Specialized Mx1 or 1xN matrix class derived from Matrix to provide row or column vectors.
Can perform dot product and cross product

### PosVector
Helper class to convert a Pos object to a column vector so it can be rotated with a 3x3 matrix