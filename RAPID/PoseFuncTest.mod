%%%
  VERSION:1
  LANGUAGE:ENGLISH
%%%

MODULE PoseFuncTest
  ! 180 degrees in X
  LOCAL CONST pose pose1:=[[100,250,400],[0,1,0,0]];
  ! 180 degrees in Y
  LOCAL CONST pose pose2:=[[100,250,400],[0,0,1,0]];
  ! 180 degrees in Z
  LOCAL CONST pose pose3:=[[100,250,400],[0,0,0,1]];
  ! 90 degrees in X
  LOCAL CONST pose pose4:=[[100,250,400],[0.707107,0.707107,0,0]];
  ! 90 degrees in Y
  LOCAL CONST pose pose5:=[[100,250,400],[0.707107,0,0.707107,0]];
  ! 90 degrees in Z
  LOCAL CONST pose pose6:=[[100,250,400],[0.707107,0,0,0.707107]];
  ! -90 degrees in X
  LOCAL CONST pose pose7:=[[100,250,400],[0.707107,-0.707107,0,0]];
  ! -90 degrees in Y
  LOCAL CONST pose pose8:=[[100,250,400],[0.707107,0,-0.707107,0]];
  ! -90 degrees in Y
  LOCAL CONST pose pose9:=[[100,250,400],[0.707107,0,0,-0.707107]];
  ! Random
  LOCAL CONST pose pose10:=[[-2347.96,1455.49,-939.22],[0.998304,-0.006946,0.021447,-0.053667]];
  PERS pose invpose1:=[[-100,250,400],[0,-1,0,0]];
  PERS pose invinvpose1:=[[100,250,400],[0,1,0,0]];
  PERS pose invpose2:=[[100,-250,400],[0,0,-1,0]];
  PERS pose invinvpose2:=[[100,250,400],[0,0,1,0]];
  PERS pose invpose3:=[[100,250,-400],[0,0,0,-1]];
  PERS pose invinvpose3:=[[100,250,400],[0,0,0,1]];
  PERS pose invpose4:=[[-100,-400,250],[0.707107,-0.707107,0,0]];
  PERS pose invinvpose4:=[[100,250,400.001],[0.707107,0.707107,0,0]];
  PERS pose invpose5:=[[400,-250,-100],[0.707107,0,-0.707107,0]];
  PERS pose invinvpose5:=[[100,250,400],[0.707107,0,0.707107,0]];
  PERS pose invpose6:=[[-250,100,-400],[0.707107,0,0,-0.707107]];
  PERS pose invinvpose6:=[[100,250,400.001],[0.707107,0,0,0.707107]];
  PERS pose invpose7:=[[-100,400,-250],[0.707107,0.707107,0,0]];
  PERS pose invinvpose7:=[[100,250,400.001],[0.707107,-0.707107,0,0]];
  PERS pose invpose8:=[[-400,-250,100],[0.707107,0,0.707107,0]];
  PERS pose invinvpose8:=[[100,250,400],[0.707107,0,-0.707107,0]];
  PERS pose invpose9:=[[250,-100,-400],[0.707107,0,0,0.707107]];
  PERS pose invinvpose9:=[[100,250,400.001],[0.707107,0,0,-0.707107]];
  PERS pose invpose10:=[[2449.15,-1211.26,1023.72],[0.998304,0.006946,-0.021447,0.053667]];
  PERS pose invinvpose10:=[[-2347.96,1455.49,-939.219],[0.998304,-0.006946,0.021447,-0.053667]];
  PERS pose pose12:=[[200,0,0],[0,0,0,1]];
  PERS pose pose23:=[[0,500,0],[0,1,0,0]];
  PERS pose pose34:=[[0,0,800],[0,0,0.707107,0.707107]];
  PERS pose pose45:=[[200,-150,650],[0.5,0.5,0.5,0.5]];
  PERS pose pose56:=[[500,500,300],[0.5,0.5,0.5,0.5]];
  PERS pose pose67:=[[-150,350,800],[0.5,-0.5,-0.5,0.5]];
  PERS pose pose78:=[[200,650,150],[0.5,-0.5,-0.5,0.5]];
  PERS pose pose89:=[[-300,500,500],[0.5,0.5,-0.5,-0.5]];
  PERS pose pose9_10:=[[1555.49,2597.96,-539.221],[0.667959,0.0102538,0.0200769,-0.743856]];
  
  PROC SimInvPoseTest()
    invpose1:=PoseInv(pose1);
    invinvpose1:=PoseInv(invpose1);
    invpose2:=PoseInv(pose2);
    invinvpose2:=PoseInv(invpose2);
    invpose3:=PoseInv(pose3);
    invinvpose3:=PoseInv(invpose3);
    invpose4:=PoseInv(pose4);
    invinvpose4:=PoseInv(invpose4);
    invpose5:=PoseInv(pose5);
    invinvpose5:=PoseInv(invpose5);
    invpose6:=PoseInv(pose6);
    invinvpose6:=PoseInv(invpose6);
    invpose7:=PoseInv(pose7);
    invinvpose7:=PoseInv(invpose7);
    invpose8:=PoseInv(pose8);
    invinvpose8:=PoseInv(invpose8);
    invpose9:=PoseInv(pose9);
    invinvpose9:=PoseInv(invpose9);
    invpose10:=PoseInv(pose10);
    invinvpose10:=PoseInv(invpose10);
  ENDPROC
  
  PROC PoseMTest()
    pose12:=PoseMult(pose1,pose2);
    pose23:=PoseMult(pose2,pose3);
    pose34:=PoseMult(pose3,pose4);
    pose45:=PoseMult(pose4,pose5);
    pose56:=PoseMult(pose5,pose6);
    pose67:=PoseMult(pose6,pose7);
    pose78:=PoseMult(pose7,pose8);
    pose89:=PoseMult(pose8,pose9);
    pose9_10:=PoseMult(pose9,pose10);
  ENDPROC
ENDMODULE
