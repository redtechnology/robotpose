﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotPose;

namespace RobotPoseExperimentConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // get pose (xyz, q1-q4) from command line
                Pose pose1 = new Pose(new Pos(100, 250, 400), new Orient(0, 1, 0, 0));
                Pose pose2 = new Pose(new Pos(100, 250, 400), new Orient(0, 0, 1, 0));
                Pose pose3 = new Pose(new Pos(100, 250, 400), new Orient(0, 0, 0, 1));
                Pose pose4 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, 0.707107, 0.000000, 0.000000));
                Pose pose5 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, 0.000000, 0.707107, 0.000000));
                Pose pose6 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, 0.000000, 0.000000, 0.707107));
                Pose pose7 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, -0.707107, 0.000000, 0.000000));
                Pose pose8 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, 0.000000, -0.707107, 0.000000));
                Pose pose9 = new Pose(new Pos(100, 250, 400), new Orient(0.707107, 0.000000, 0.000000, -0.707107));
                Pose pose10 = new Pose(new Pos(-2347.96, 1455.49, -939.22), new Orient(0.998304, -0.006946, 0.021447, -0.053667));
                WritePose("Pose 1", pose1);
                WritePose("Pose 2", pose2);
                WritePose("Pose 3", pose3);
                WritePose("Pose 4", pose4);
                WritePose("Pose 5", pose5);
                WritePose("Pose 6", pose6);
                WritePose("Pose 7", pose7);
                WritePose("Pose 8", pose8);
                WritePose("Pose 9", pose9);
                WritePose("Pose 10", pose10);

                Pose pose12 = Geometry.PoseMult(pose1, pose2);
                Pose pose23 = Geometry.PoseMult(pose2, pose3);
                Pose pose34 = Geometry.PoseMult(pose3, pose4);
                Pose pose45 = Geometry.PoseMult(pose4, pose5);
                Pose pose56 = Geometry.PoseMult(pose5, pose6);
                Pose pose67 = Geometry.PoseMult(pose6, pose7);
                Pose pose78 = Geometry.PoseMult(pose7, pose8);
                Pose pose89 = Geometry.PoseMult(pose8, pose9);
                Pose pose910 = Geometry.PoseMult(pose9, pose10);

                WritePose("pose12", pose12);
                WritePose("pose23", pose23);
                WritePose("pose34", pose34);
                WritePose("pose45", pose45);
                WritePose("pose56", pose56);
                WritePose("pose67", pose67);
                WritePose("pose78", pose78);
                WritePose("pose89", pose89);
                WritePose("pose910", pose910);

                Pose inverse10 = Geometry.PoseInv(pose10);
                Pose inverseinverse10 = Geometry.PoseInv(inverse10);
                WritePose("Pose 10", pose10);
                WritePose("Inverse pose 10", inverse10);
                WritePose("Inverse inverse pos", inverseinverse10);

                //string isOK = "*** YES ***";

                //if (backToOriginal != original)
                //{
                //    isOK = "### NO ###";
                //}
                //Console.WriteLine("{0}", isOK);

                //TestMultiplyTwoUnitQuaternionAndGetUnitQuaternionBack();
                
                //TestMultiplyTwo90DegreeQuaternions();
                
                //TestMultiplyTwoTypicalQuaternions();

                //TestPoseMult();

                TestMatrixMultiply();

                TestMatrixTranspose();
            }

            catch (Exception e)
            {

                Console.WriteLine("There was a problem: {0}", e.Message);
            }

            finally
            {
                Console.WriteLine("Press any key to close...");
                Console.ReadKey();
            }
        }

        public static void WritePose(string title, Pose pose)
        {
            Console.WriteLine("{0}:", title);

            Console.WriteLine("Pos: X: {0} Y: {1} Z: {2}",
                pose.Pos.X,
                pose.Pos.Y,
                pose.Pos.Z);

            Console.WriteLine("Orient: q1: {0} q2: {1} q3: {2} q4: {3}",
                pose.Orient.Q1,
                pose.Orient.Q2,
                pose.Orient.Q3,
                pose.Orient.Q4);
        }

        public static void TestMultiplyTwoUnitQuaternionAndGetUnitQuaternionBack()
        {
            Console.WriteLine("TestMultiplyTwoUnitQuaternionAndGetUnitQuaternionBack...");

            Orient unitQuaternion = new Orient(1, 0, 0, 0);

            Orient first = new Orient(unitQuaternion);

            Orient second = new Orient(unitQuaternion);

            Orient resultant = first * second;

            Console.WriteLine("Orient: q1: {0} q2: {1} q3: {2} q4: {3}",
                resultant.Q1,
                resultant.Q2,
                resultant.Q3,
                resultant.Q4);
        }

        public static void TestMultiplyTwo90DegreeQuaternions()
        {
            Console.WriteLine("TestMultiplyTwo90DegreeQuaternions...");

            Orient first = new Orient(0.707167, 0.707167, 0, 0);

            Orient second = new Orient(0.707167, 0.707167, 0, 0);

            Orient resultant = first * second;

            Console.WriteLine("Orient: q1: {0} q2: {1} q3: {2} q4: {3}",
                resultant.Q1,
                resultant.Q2,
                resultant.Q3,
                resultant.Q4);
        }

        public static void TestMultiplyTwoTypicalQuaternions()
        {
            Console.WriteLine("TestMultiplyTwoTypicalQuaternions...");

            Orient first = new Orient(0.402811, -0.62219, 0.597408, -0.30615);

            Orient second = new Orient(0.538383, -0.531347, 0.518617, -0.398561);

            Orient resultant = first * second;

            Console.WriteLine("Orient: q1: {0} q2: {1} q3: {2} q4: {3}",
                resultant.Q1,
                resultant.Q2,
                resultant.Q3,
                resultant.Q4);
        }

        public static void TestPoseMult()
        {
            Console.WriteLine("TestMultiplyTwoTypicalQuaternions...");

            Pos firstPos = new Pos(100, 200, 300);
            Orient firstOrient = new Orient(0.402811, -0.62219, 0.597408, -0.30615);
            
            Pose first = new Pose(firstPos, firstOrient);

            Pos secondPos = new Pos(1000, 2000, 3000);
            Orient secondOrient = new Orient(0.538383, -0.531347, 0.518617, -0.398561);

            Pose second = new Pose(secondPos, secondOrient);

            Pose resultant = Geometry.PoseMult(first, second);

            WritePose("PoseMult", resultant);
        }

        public static void TestMatrixMultiply()
        {
            Console.WriteLine("Test Matrix Multiply...");

            double[,] a = { { 1, 2, 3}, {4, 5, 6} };
            double[,] b = { { 7, 8 }, {9, 10}, {11, 12} };

            Matrix A = a;
            Matrix B = b;

            PrintMatrix(A);

            Console.WriteLine();
            
            PrintMatrix(B);

            Matrix C = A * B;

            PrintMatrix(C);
        }

        public static void TestMatrixTranspose()
        {
            Console.WriteLine("Test Matrix Transpose...");

            double[,] a = { { 1, 2, 3 }, { 4, 5, 6 } };

            Matrix A = a;
            Matrix B = A.Transpose();

            PrintMatrix(A);

            Console.WriteLine();

            PrintMatrix(B);
        }

        public static void PrintMatrix(Matrix matrix)
        {
            for (int rowCounter = 0; rowCounter < matrix.Rows; rowCounter++)
            {
                for (int columnCounter = 0; columnCounter < matrix.Columns; columnCounter++)
                {
                    System.Console.Write("{0} ", matrix[rowCounter, columnCounter]);
                }

                System.Console.WriteLine();
            }
        }
    }
}
